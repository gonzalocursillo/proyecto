﻿const database = 'http://192.168.100.71:5984/gon_libros/'

var defaultAjax={
	type: 'get',
	url: database + '/_all_docs?include_docs=true',
	dataType: 'json',
	contentType: 'application/json; charset=utf-8'
	}

function getBooks(){
	defaultAjax.type = 'get'
	defaultAjax.url = database + '_all_docs?include_docs=true',
	$.ajax(defaultAjax).done(function(data){
		$('#selector').html('')
		$.each(data.rows,function(idx,row){
			console.log(row.doc._id, row.doc.libro, row.doc.autor, row.doc.tematica, row.doc.isbn);
			let html =`
			<option value="${row.id}">
			${row.doc.libro}, ${row.doc.autor}, ${row.doc.tematica}, ${row.doc.isbn}
			</option>
			`
			$('#selector').append(html)
		});

	});
}

function crear(){
	var obj = { 
		libro: $('#libro').val(), 
		autor: $('#autor').val(),
		tematica: $('#tematica').val(),
		isbn: $('#isbn').val(),
	}
	$.ajax({type: 'post', 
		url: database , 
		data: JSON.stringify(obj) ,
		dataType: 'json',
		contentType: 'application/json; charset=utf-8'})
		.done( function(data){ getBooks()	})
		.fail( function(data){ alert( JSON.parse(data.responseText).reason    ) })	

}



function cambiaCombo(){

	defaultAjax.url = database + $('#selector option:selected').val()
	defaultAjax.type = 'get'
	$.ajax(defaultAjax).done(function(data){
		$('#id').val(data._id)
		$('#rev').val(data._rev)
		$('#libro').val(data.libro)
		$('#autor').val(data.autor)
		$('#tematica').val(data.tematica)
		$('#isbn').val(data.isbn)
	})
}

function modificar(){

	$.ajax({
		type: 'post',
		url: database,
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify({
			_id: $('#id').val(), 
			_rev: $('#rev').val(),
			libro: $('#libro').val(),
			autor: $('#autor').val(),
			tematica: $('#tematica').val(),
			isbn: $('#isbn').val(),
			})		
	})
	getBooks()
}

function borrar(){

	$.ajax({
		type: 'DELETE',
		url: database + $('#id').val() + "?rev=" + $('#rev').val(),
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify({
			_id: $('#id').val(), 
			_rev: $('#rev').val(),
			libro: $('#libro').val(),
			autor: $('#autor').val(),
			tematica: $('#tematica').val(),
			isbn: $('#isbn').val(),
			})

	})
	getBooks()
}
	
$(document).ready( getBooks )
