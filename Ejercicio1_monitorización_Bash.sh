#!/bin/bash


echo "Cuánto tiempo lleva encendido el PC";
echo -----------------------------------;
uptime;
echo "Pulsa intro para continuar";
read;

echo "Información general sobre la cpu";
echo --------------------------------;
cat /proc/cpuinfo;
echo "Pulsa intro para continuar";
read;

echo "En concreto queremos saber el número de procesadores";
echo ----------------------------------------------------;
cat /proc/cpuinfo | grep processor;
echo "Pulsa intro para continuar";
read;

echo "Información general sobre la memoria";
echo ------------------------------------;
cat /proc/meminfo;
echo "Pulsa intro para continuar";
read;

echo "Resumen de la memoria";
echo ---------------------;
free;
echo "Pulsa intro para continuar";
read;

echo "Espacio libre en los discos";
echo ---------------------------;
df -h;
echo "Pulsa intro para continuar";
read;


echo "Mostremos la arquitectura del ordenador";
echo ---------------------------------------;
arch;
echo "Pulsa intro para continuar";
read;

echo "Veamos los dispositivos pci conectados";
echo --------------------------------------;
lspci;
echo "Pulsa intro para continuar";
read;

echo "Veamos ahora los dispositivos usb";
echo ---------------------------------;
lsusb;
echo "Pulsa intro para continuar";
read;

echo "Controladores de los dispositivos";
echo ---------------------------------;
cat /proc/devices;
echo "Pulsa intro para continuar";
read;

echo "Tabla de particiones (necesaria contraseña de root)";
echo ---------------------------------------------------;
sudo fdisk -l;
echo "Pulsa intro para continuar";
read;

echo "Por último los servicios activos e inactivos (necesaria contraseña de root)";
echo ---------------------------------------------------------------------------;
sudo service --status-all;
echo "Pulsa intro para continuar";
read;

clear;
echo "¡Gracias por usar este programa!";
exit;