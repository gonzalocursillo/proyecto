var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var port = 3000;
var mysql = require('mysql');
var connection = mysql.createConnection({
	host: '192.168.100.71',
	user: 'root',
	password: 'a',
	database: 'gon'
});
connection.connect();

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}) );

app.get('/libros', function(req,res){
	connection.query('select * from libros;', function(error,result){
		res.json(result);
	});
});

app.get('/libro/:id', function(req,res){
	connection.query('select * from libros where isbn = ?;', [req.params.id], function(error,result){
		res.json(result[0]);
	});
});

app.post('/libro/', function(req,res){
	connection.query('insert into libros (titulo, autor, genero, isbn) values(?,?,?,?);', 
		[req.body.titulo, req.body.autor, req.body.genero, req.body.isbn],
		function(error,result){			
			res.json(result);			
	});
});		
app.delete('/libro/:id',function(req,res){
	let sql=`delete from libros where isbn = ? ;`
	connection.query(sql, [req.params.id], function (error,result) {
		res.json(result);
	});
});
app.post('/updatelibro/',function(req,res){
	let sql=`UPDATE libros SET titulo = ?, autor = ?, genero = ? WHERE isbn = ?;`
	// connection.query('SET sql_safe_updates=0')
	connection.query(sql, [req.body.titulo, req.body.autor, req.body.genero, req.body.isbn], function (error,result) {
		console.log(error);
	});
});
app.post('/libro/:id',function(req,res){
	response.json(req.body)
});

app.listen(port,function(){
	console.log('mysql middleware escuchando los Beatles en puerto '+ port);
});
