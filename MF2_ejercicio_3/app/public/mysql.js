﻿const servermid = 'http://127.0.0.1:3000/';

var defaultAjax={
	type: 'get',
	url: servermid + 'gon',
	dataType: 'json',
	contentType: 'application/json; charset=utf-8'
	}

function getLibro(){
	defaultAjax.type = 'get',
	defaultAjax.url = '/libros',
	$.ajax(defaultAjax).done(function(data){
		$('#selector').html('')
		$.each(data,function(idx,row){
			//console.log(row.doc._id, row.doc.libro, row.doc.autor, row.doc.tematica, row.doc.isbn);
			let html =`
			<option value="${row.isbn}">
			${row.titulo}, ${row.autor}, ${row.genero}, ${row.isbn}
			</option>
			`
			$('#selector').append(html)
		});

	});
}

function newLibro(){
	var obj = { 
		titulo: $('#titulo').val(), 
		autor: $('#autor').val(),
		genero: $('#genero').val(),
		isbn: $('#isbn').val(),
	}
	$.ajax({
		type: 'post', 
		url: '/libro/' , 
		data: JSON.stringify(obj) ,
		dataType: 'json',
		contentType: 'application/json; charset=utf-8'
	})
	.done( function(data){			
			getLibro()
	})
	.fail( function(err){ console.log(err) } )
}			



function rellena(){

	$.ajax({
		url: '/libro/' + $('#selector option:selected').val(),
		type: 'get',
		success: function(data){ 
			//console.log(data)
			$('#titulo').val(data.titulo)
			$('#autor').val(data.autor)
			$('#genero').val(data.genero)
			$('#isbn').val(data.isbn)	
		}
	})
	/*
	.done(function(data){
		console.log(data)
		//$('#id').val(data._id)
		//$('#rev').val(data._rev)
		$('#libro').val(data.titulo)
		$('#autor').val(data.autor)
		$('#tematica').val(data.genero)
		$('#isbn').val(data.isbn)
	})
	.fail( function(err){ console.log(err)})
	*/
}

function updateLibro(){

	$.ajax({
		type: 'post',
		url: '/updatelibro/',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify({
			//_id: $('#id').val(), 
			//_rev: $('#rev').val(),
			titulo: $('#titulo').val(),
			autor: $('#autor').val(),
			genero: $('#genero').val(),
			isbn: $('#isbn').val(),
			})		
	})
	.done( function(data){ 
		console.log(data) 
		getLibro()
	})
	.fail( function(err) {console.log(err)})
}

function deleteLibro(){

	$.ajax({
		type: 'delete',
		url: '/libro/' + $('#isbn').val(),
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify({
			//_id: $('#id').val(), 
			//_rev: $('#rev').val(),
			titulo: $('#titulo').val(),
			autor: $('#autor').val(),
			genero: $('#genero').val(),
			isbn: $('#isbn').val(),
			})

	})
	getLibro()
}
	
$(document).ready(getLibro())
