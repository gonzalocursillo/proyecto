import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-modal';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ProductosComponent } from './productos/productos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as firebase from 'firebase';
import { TestComponent } from './test/test.component';


const firebaseProject: object = {
  apiKey: "AIzaSyDuerqiEIsK8rcQ1WwF_y0RZooesYL7UJw",
  authDomain: "proyecto-clase-3e553.firebaseapp.com",
  databaseURL: "https://proyecto-clase-3e553.firebaseio.com",
  projectId: "proyecto-clase-3e553",
  storageBucket: "proyecto-clase-3e553.appspot.com",
  messagingSenderId: "607812745853"
};

const appRoutes: Routes = [  
  { path: 'productos', component: ProductosComponent},
  { path: 'clientes', component: ClientesComponent },
  { path: 'home', component: HomeComponent},
  { path: 'test', component: TestComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full' },
]


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClientesComponent,
    ProductosComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,    
    AngularFireModule.initializeApp(firebaseProject),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot(appRoutes),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
