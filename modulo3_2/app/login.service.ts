import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';



@Injectable()
export class LoginService {
    user: any;
    constructor(private af: AngularFireAuth, private db: AngularFireDatabase) {
    af.auth.onAuthStateChanged( user => {
        if (user) {this.setProfile(user) }
    });
}

setProfile(user) {
    this.user = user;
    this.db.object('/clientes/').update({
        uid: user.uid,
        nombre: user.nombre,
        apellido: user.apellido,
        direccion: user.direccion,
    })
}
loguear() {
    return this.af.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    }
logout(){
    console.log ("Te has desconectado correctamente");
    return this.af.auth.signOut();
    }
}