import { Component, OnInit } from '@angular/core';
//import { ClienteService } from '../cliente.service';
import { NgForm } from '@angular/forms';
import { Usuario } from '../admin/usuario.model';
import { AngularFireDatabaseModule, AngularFireList, AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css'],
  
})
export class ClientesComponent implements OnInit {
  listaUsuarios: AngularFireList<any>;
  usuarioSeleccionado: Usuario = new Usuario
  
  constructor(private firebase: AngularFireDatabase) { }
  onSubmit3(form3: NgForm){
    this.neo(form3.value);
    
    

  }
  neo(usuario: Usuario) {
    this.listaUsuarios = this.firebase.list('/users/');
    this.listaUsuarios.push({
      
      nombre: usuario.displayName,
      apellidos: usuario.displaySurname,
      direccion: usuario.direccion,
      codigo: usuario.codigoPo,
      provincia: usuario.provincia,
      telefono: usuario.telefono,
      email: usuario.email,
      
    });
  }

  

  ngOnInit() {
  }

}
