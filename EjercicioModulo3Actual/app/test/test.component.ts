import { Component, OnInit } from '@angular/core';

declare var pdfMake: any;

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  verpdf(value){
    let presupuesto = {
      cliente: 'Paco',
      CIF: 5087885
    }
    let pdf: any = pdfMake;
    let docDefinition = {
      content: [],
      footer: []
    }
    docDefinition.content.push({ text: presupuesto.cliente})
    docDefinition.content.push({ text: presupuesto.CIF})
    pdf.createPdf(docDefinition).open()
  }
  
}
