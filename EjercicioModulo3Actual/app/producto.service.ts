import { Injectable } from '@angular/core';
import { Producto } from './admin/producto.model';
import { AngularFireDatabaseModule, AngularFireList, AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ProductoService {
  listaProductos: AngularFireList<any>;
  productoSeleccionado: Producto = new Producto

  constructor(private firebase: AngularFireDatabase) { }

  nuevo(producto: Producto) {
    this.listaProductos = this.firebase.list('/productos/');
    this.listaProductos.push({
      descripcion: producto.descripcion,
      //foto: producto.foto,
      nombre: producto.nombre,
      peso: producto.peso,
      precio: producto.precio,
      tipo: producto.tipo
    });
  }
}
