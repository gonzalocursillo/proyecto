import { Injectable } from '@angular/core';
import { Cliente } from './admin/cliente.model';
//import { Usuario } from './admin/usuario.model';
import { AngularFireDatabaseModule, AngularFireList, AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ClienteService {
  listaClientes: AngularFireList<any>;
  //listaUsuarios: AngularFireList<any>;
  clienteSeleccionado: Cliente = new Cliente;
  //usuarioSeleccionado: Usuario = new Usuario;
  

  constructor(private firebase: AngularFireDatabase) { }

  nuevo(cliente: Cliente) {
    this.listaClientes = this.firebase.list('/clientes/');
    this.listaClientes.push({
      nombre: cliente.nombre,
      apellido: cliente.apellido,
      direccion: cliente.direccion,
    });
  }
  /*neo(usuario: Usuario) {
    this.listaUsuarios = this.firebase.list('/users/');
    this.listaUsuarios.push({
      nombre: usuario.displayName,
      apellidos: usuario.displaySurname,
      direccion: usuario.direccion,
      provincia: usuario.provincia,
      codigo: usuario.codigoPo,
      email: usuario.email,
      telefono: usuario.telefono,
    });
  }*/
  
  /*modifica2(cli: Cliente) {
    
    this.col.update(cli.$key,{
      nombre: cli.nombre,
      apellido: cli.apellido,
      direccion: cli.direccion
    });

  }*/

}
