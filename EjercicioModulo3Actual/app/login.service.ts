import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { FormControl, Validators } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable'


@Injectable()
export class LoginService {
    user: any;
    constructor(private af: AngularFireAuth, private db: AngularFireDatabase) {
    af.auth.onAuthStateChanged( user => {
        if (user) {this.setProfile(user) }
    });
}
 

setProfile(user) {
    this.user = user;
    this.db.object('/clientes/').update({
        uid: user.uid,
        nombre: user.displayName,
        //apellido: user.apellido,
        //direccion: user.direccion,
    })
}
loguear() {
    return this.af.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    
    }
logout(){
    alert ("Te has desconectado correctamente");
    return this.af.auth.signOut();
    }

}
/*export class Producto {  
    public coleccion: string = '/productos/'
    private data: AngularFireList<any[]>; 
    constructor( 
      private anDb: AngularFireDatabase,
      public ngAuth: AngularFireAuth ){
      this.data = anDb.list(this.coleccion)//,{ query: { orderByChild: 'tipo' }});     
    }
}*/