import { Component, OnInit } from '@angular/core';
import { ModalModule } from 'ngx-modal';
//import { ProductosComponent } from '../productos/productos.component';
import { AngularFireModule } from 'angularfire2';
import {  AngularFireDatabaseModule, AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductoService } from '../producto.service'
import { ClienteService } from '../cliente.service'
import { NgForm } from '@angular/forms'
import { Producto } from './producto.model'
import { Cliente } from './cliente.model'
import { Usuario } from './usuario.model';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers : [ProductoService, ClienteService]
})
export class AdminComponent {
  listaProductos: AngularFireList<Producto>;
  listaClientes: AngularFireList<Cliente>;
  rtPath: string ='/productos/';
  rtPath2: string ='/clientes/';
  rtDB: AngularFireList<any[]>;
  rtDB2: AngularFireList<any[]>;
  
  productos: Observable<any[]>;
  clientes: Observable<any[]>;

  constructor (private ngDb: AngularFireDatabase, private productoService: ProductoService, private clienteService: ClienteService){
    this.rtDB = ngDb.list('/productos');
    this.productos = this.rtDB.valueChanges();
    this.rtDB2 = ngDb.list('/clientes');
    // this.clientes = this.rtDB.snapshotChanges();
    this.productos = this.rtDB.snapshotChanges().map(changes => {
      return changes.map(p => ({ key: p.payload.key, ...p.payload.val() }) );
    });
    this.clientes = this.rtDB2.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }) );
    });

  }
  borra1(id){
    this.rtDB.remove(id);
    
  }
  
  borra2(id){
    this.rtDB2.remove(id);
    
  }
  /*nuevo(form1: any): void {
    this.ngDb.list('/clientes').push(form1.value)
  }*/
  onSubmit(form1: NgForm){
    this.productoService.nuevo(form1.value);
    this.resetea1(form1);
    

  }
  onSubmit2(form2: NgForm){
    this.clienteService.nuevo(form2.value);
    this.resetea2(form2)
  }
  /*onSubmit3(form3: NgForm){
    this.clienteService.neo(form3.value);
    
    

  }*/
  cambia1(p) {
    console.log(p)
    this.rtDB.update(p.key,p);
  }

    cambia2(c) {
    console.log(c)
    this.rtDB2.update(c.key,c);
  }
  
  resetea1(form1?: NgForm) {
    if(form1!= null)
    form1.reset();
    this.productoService.productoSeleccionado = {
      $key: '',
      nombre: '',
      peso: '',
      precio: '',
      tipo: '',
      descripcion: '',
      foto: ''
    }
  }
  resetea2(form2?: NgForm) {
    if(form2!= null)
    form2.reset();
    this.clienteService.clienteSeleccionado = {
      $key: '',
      nombre: '',
      apellido: '',
      direccion: ''
    }
  }
  seleccionado(pro: Producto) {
    this.productoService.productoSeleccionado = pro;
      }
  seleccionado2(cli: Cliente) {
    this.clienteService.clienteSeleccionado = cli;
      }
      /*uploadFile(file: File, carpeta: string): Promise<string> {    
        //let path = this.folderName(firebase.storage)
        return new Promise( ( resolve, reject ) => {
          //if ( path ){
            //let ref = firebase.storage().ref(path);
            let task: firebase.storage.UploadTask = firebase.storage().ref(carpeta).
            child(file.name).put(file);
            task.on(firebase.storage.TaskEvent.STATE_CHANGED,
              (data) => {  },
              (error) => {  },
              () => {                    
                resolve(task.snapshot.downloadURL);          
              })
            })
          }*/
          
          /*graba(file){
            
            let task: firebase.storage.UploadTask =
            firebase.storage().ref('').child(file.name).put(file);
            task.on(firebase.storage.TaskEvent.STATE_CHANGED,
            (data: any)=>{},
            ()=>{},
            ()=>{}
            )
          }*/
          /*upload() {
            this.files.forEach(file =>
            firebase.storage().ref('').child(file.name).put(file))
          }*/
}
