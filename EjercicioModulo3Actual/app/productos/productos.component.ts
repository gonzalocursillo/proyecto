import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import {  AngularFireDatabaseModule, AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import { ModalModule } from 'ngx-modal';
import { ProductoService } from '../producto.service'

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css'],
  providers : [ProductoService]
  
})
export class ProductosComponent {
  carrito: Array<any>[];
  
  //items: Observable<any>
  /*nombre: string
  edad: number
  productos = [
    {nombre: 'Pepe', edad: 15},
    {nombre: 'Juan', edad: 23}


  ]*/
  rtPath: string ='/productos/';
  rtDB: AngularFireList<any[]>;
  productos: Observable<any[]>;
  producSelec: any;

  constructor (private ngDb: AngularFireDatabase){
    this.rtDB = ngDb.list('/productos')
    this.productos = this.rtDB.valueChanges();
  }
  addCarro(p) {
    this.carrito.push(p);
    console.log(this.carrito)
  }

    //public anDb: AngularFireDatabase
    //productos: Array<any>
    //private productos: Array<any['/clientes/']> 

  
    //private productos: Observable<any[]>
    //prArticulos: Observable<any[]>;
    //Array<any>=[ 'pepe' 'lopez']
    //constructor( /*private productos: Array<any['/clientes/']>*/ ){
     /* this.productos = this.anDb.list('/clientes/').valueChanges()
    }*/
      //this.productos = anDb.list(this.coleccion).valueChanges();
      //this.prArticulos = this.productos.valueChanges();
      //private productos: anDb.list('/productos/')
      //console.log(this.data)   
    //};

  //ngOnInit() {
  //}

//}

  
  }